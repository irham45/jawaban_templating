<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form(){
    	return view('form');
    }

    public function sapa(){
    	return "selamat datang ";
    }

    public function sapa_post(Request $request){
    	//dd($request->all());
    	//return "ini adalah post";
    	$nama = $request["nama"];
    	return "$nama";
    }

    public function index(){
    	return view('index');
    }

}
